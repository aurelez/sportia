# Sportia App
##### Author: Aurele Zannou - ZAND17058708
INF4375 - Aut 2017
> Le projet consiste à récupérer un ensemble de données provenant de la ville de Montréal et d'offrir des services à partir de ces données. Il s'agit de données ouvertes à propos d'installations pour faire des activités sportives. Les nouvelles installations détectées lors d'un cron-task sont automatiquement publiées sur le compte twitter: https://twitter.com/AureleZ

## Installation
##### Requis:
- Node.JS
- NPM
- MongoDB

##### Procédure
* ***Production:*** Installer les dependencies

```sh
$ cd sportia
$ npm install --only=prod
$ npm start
```
La ligne (npm start) peut être remplacée par (node ./bin/www)

* ***Développement:*** Installer les dependencies et les devDependencies: DEBUG mode

```sh
$ cd sportia
$ npm install
$ npm run dev
```
##### Configs file
le fichier ***configs.yml*** à la racine du projet contient tous les paramàtres nécéssaires à la configuration de l'app (Courriel d'admin, liens des installations, paramètres de la BD, local et remote etc..). Note:
- le paramètre ```importationCronTaskFrequence``` détermine la fréquence d'importation des données
- le paramètre ```isonline``` de l'objet app détermine si l'application est utilisée en local ou en ligne
- le paramètre ```useremote``` de l'objet bdd détermine si l'application utilise une BD local ou celle en ligne Ex: mLab
- ***Par défaut:*** l'app est configurée pour fonctionner en local

##### Routes (end-user)
- ***/*** (Accueil, recherches)
- ***/doc*** (Documentation)
- ***/logs*** (Errors/Notifications Logs)
- ***/auth/twitter*** (Authentification avec Twitter)

# Fonctionnalités
Listes des fonctionnalités disponibles.

## A1
Importation des données de la ville de Montréal dans une BD Mongo
- Une importation des données est faite ***par défaut*** au démarrage de l'app. 
- Consulter la base de données Mongo, une nouvelle BD ***Sportia*** devrait y apparaitre. Les installations seront disponibles dans la collection Installation, et les logs dans la collection logs

## A2
Importation de données automatiquement chaque jour à minuit
- le paramètre ```importationCronTaskFrequence: '0 0 0 * * *'``` dans le fichier ***configs.yml*** à la racine détermine la fréquence d'importation des données. 
- Le script d'importation des données se trouve dans le répertoire ***/inc/importationProcess.js***

## A3
Accessibilité de la documentation de l'API
- La documentation est disponible sur la route ***/doc***
- Le fichier RAML se trouve dans le repertoire: ***views/apiDoc.raml***

## A4
Service REST pour la liste des installations pour un arrondissement
- ***Note:*** Tous les services REST se trouvent sur la route ***/api***
- Service A4: ```/api/installations?arrondissement=LaSalle```

## A5
Recherche ajax par arrondissement à partir d'un formulaire HTML
- Disponible sur la route ***/***
- Entrer le nom d'un arrondissement dans la search bar, des suggestions sont affichées en real time selon les caractères entrées

## A6
Recherche ajax par non d'installation (liste déroulante)
* Disponible sur la route ***/***
* Utiliser le dropdown dans la section ***Recherche par installation***

## B1
Le système détecte les nouvelles installations depuis la dernière importation de données, en dresse une liste sans doublon et l'envoi par courriel automatiquement.
- Cette tâche est éffectuée à chaque importation
- Le courriel admin peut être changé dans le fichier ***configs.yml***
- ```adminMail: 'admin@gmail.com' ```

## B2
Les noms des nouvelles installations sont publiés automatiquement sur un compte Twitter.
- les paramètres de l'api twitter sont disponibles dans le fichier ***configs.yml***
- le compte twitter: ```https://twitter.com/AureleZ```

## C1
Le système offre un service REST permettant d'obtenir la liste des installations en mauvaise condition. (liste triée)
- le service est disponible sur la route ```/api/badconditioninstallations/json```

## C2
Le système offre un service permettant d'obtenir exactement les mêmes données que le point C1 mais
en format XML
- le service est disponible sur la route ```/localfiles/badconditionpiscines.xml```

## C3
Le système offre un service permettant d'obtenir exactement les mêmes données que le point C1 mais
en format CSV
- le service est disponible sur la route ```/localfiles/badconditionpiscines.csv```

## D1
Le système offre un service REST permettant de modifier l'état d'une glissade. Le client doit envoyer
un document JSON contenant les modifications à apporter à la glissade. Le document JSON doit être
validé avec json-schema.
- le service est disponible sur la route ```/api/installation/update/{id}```
- le paramètre {id} représente l'id mongoDB de l'installation à modifier
- Exemple d'appel (***put***):  ```/api/installation/update/5a08553e85fc5f1104507170```
- Exemple de body:
```
{
  "nom": "Nouveau nom",
  "arrondissement": "Agblanganda",
  "cle_arr": "agbd",
  "condition": "Mauvaise",
  "date_maj": "2017-03-04T14:50:08.000Z",
  "deblaye": "0",
  "ouvert": "1"
}
```

## D2
Le système offre un service REST permettant de supprimer une glissade.
- le service est disponible sur la route ```/api/installation/delete/{id}```
- le paramètre {id} représente l'id mongoDB de l'installation à supprimer
- Exemple d’appel (***delete***): /api/installation/delete/5a08553e85fc5f1104507170

## D3
Supprimer ou modifier les installations (Piscines, Glissades, Patinoires) retournées par l'outil de recherche du point A5. L'application invoque les services faits en D1 et D2 avec des appels Ajax.
- ***Note:*** Pour avoir accès à cette fontionnalité, il faut d'abord se connecter avec un compte twitter
- Pour forcer l'accès à cette fonctionnalité sans avoir à se connecter à un compte twitter, il faudra mettre le paramètre ***forceAccessToAdminOptions*** à ***true*** dans le fichier de configuration ***configs.yml*** Exemple: ```forceAccessToAdminOptions: true```
- les options de Modification et suppression apparraissent en dessous de chaque installation retournée par la recherche par arrondissement

## D4
Le système offre une procédure d'authentification du type « Login with Twitter » et permet de restreindre l'accès aux fonctionnalités de modification et suppression de D3
- L'authetification par Twitter est disponible sur la route ```/auth/twitter``` boutton ***Twitter Login*** en haut à droite dans le menu. 
- Les utilisateurs sont stockés dans la table ***users***

## F1
Le système est entièrement déployé sur la plateforme infonuagique Heroku
- lien Heroku: ```https://sportia.herokuapp.com```
- Heroku login:
***user:*** aureluss@live.com
***pass:*** Katana87
- Remode BD (mLab) : ```https://mlab.com```
```
{
    name: 'sportia',
    host: 'ds029486.mlab.com',
    port: 29486,
    username: 'aureluss',
    password: 'katmandu'
}
```
- BD mLab account login
***user:*** aureluss
***pass:*** Katana87