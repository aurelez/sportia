/*	Projet: NF4375 - Aut 2017
 	Author: Aurele Zannou - ZAND17058708
*/
const mongoose = require('mongoose');
var Schema = mongoose.Schema;

var logSchema = new Schema({
	twitterId: "number",
	username: String,
	displayname: String,
	photo: String
});

module.exports = mongoose.model('users', logSchema);