/*	Projet: NF4375 - Aut 2017
 	Author: Aurele Zannou - ZAND17058708
*/
const mongoose = require('mongoose');
var Schema = mongoose.Schema;

var installationSchema = new Schema({
	nom: {type: String},
	type: {type: String},
	arrondissement: {type: String},
	condition: {type: String},
	data: {type: Schema.Types.Mixed}
});

module.exports = mongoose.model('installations', installationSchema);