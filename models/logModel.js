/*	Projet: NF4375 - Aut 2017
 	Author: Aurele Zannou - ZAND17058708
*/
const mongoose = require('mongoose');
var Schema = mongoose.Schema;

var logSchema = new Schema({
	type: String,
	desc: String,
	date: {type: Date, default: new Date().toLocaleString()}
});

module.exports = mongoose.model('logs', logSchema);