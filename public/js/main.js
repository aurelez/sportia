/*	Projet: NF4375 - Aut 2017
 	Author: Aurele Zannou - ZAND17058708
*/

Vue.prototype.$http = axios
var sportiapp = new Vue({
	el: '#sportiapp',
	data: {
		instasearch: '',
		showspinner: false,
		installationsByArr: [],
		msgerror: '',
		arrondNames: [],
		filterArrNames: [],
		showlivesearchbox: false,
		installationsNames: [],
		installationsfound: [],
		installationtoOperate: {},
		installationByIdApiUri: window.location.origin+'/api/installationbyid/',
		installationsByNameApiUri: window.location.origin+'/api/installationbyname/',
		searchByArrApiUri: window.location.origin+'/api/installations?arrondissement=',
		distinctsArrondApiUri: window.location.origin+'/api/distinctsarrond',
		installationsNamesApiUri: window.location.origin+'/api/installationsnames',
		updateInstallationApiUri: window.location.origin+'/api/installation/update/',
		deleteInstallationApiUri: window.location.origin+'/api/installation/delete/',
		formMessage: {failed: false, msgs: null},
		updatefield: false,
		showModalDelbtn: true
	},

	computed: {
	},

	beforeMount(){
		var vm = this
		this.$http.get(this.distinctsArrondApiUri)
			.then(function (response) {
				Vue.set(vm.$data, 'arrondNames', response.data)
			}).catch(error => { console.log(error.response.data) });

		this.$http.get(this.installationsNamesApiUri)
			.then(function (response) {
				Vue.set(vm.$data, 'installationsNames', response.data)
			}).catch(error => { console.log(error.response.data) });
	},

	methods: {
		//--computedArrlist
		arrlistLiveSearch: function(){
			var vm = this
			results = this.arrondNames.filter(function (item) {
				return item.toLowerCase().indexOf(vm.instasearch.toLowerCase()) >= 0 && vm.instasearch.length > 0
			})
			//console.log(results)
			results.length > 0 ? Vue.set(vm.$data, 'showlivesearchbox', true) : Vue.set(vm.$data, 'showlivesearchbox', false)
			Vue.set(vm.$data, 'filterArrNames', results.slice(0, 6))
		},

		//--searchByArr
		searchByArr: function(){
			var vm = this
			this.showspinner = true;

			this.$http.get(this.searchByArrApiUri+encodeURIComponent(this.instasearch))
				.then(function (response) {
					Vue.set(vm.$data, 'showspinner', false)
					Vue.set(vm.$data, 'installationsByArr', response.data)
					if(response.data.length == 0){
						Vue.set(vm.$data, 'msgerror', 'Aucune installation trouvée pour cette cet arrondissement')
					}else{Vue.set(vm.$data, 'msgerror', '')}
				})
				.catch(error => {
					Vue.set(vm.$data, 'installationsByArr', [])
					Vue.set(vm.$data, 'showspinner', false)
					Vue.set(vm.$data, 'msgerror', error.response.data.message)
				});
		},

		//--searchclic
		liveSearchClicked: function(name){
			var vm = this
			Vue.set(vm.$data, 'instasearch', name)
			Vue.set(vm.$data, 'showlivesearchbox', false)
			this.searchByArr()
		},

		//-- searchByInstallation
		searchByInstallation: function(event){
			var vm = this
			//console.log(event.target.value)
			this.$http.get(this.installationsByNameApiUri+encodeURIComponent(event.target.value))
				.then(function (response) {
					Vue.set(vm.$data, 'installationsfound', response.data)
					//console.log(response.data)
				}).catch(error => { console.log(error) });
		},

		//--updateInstallationInit
		updateInstallationInit: function(inst){
			this.updatefield = true;
			this.formMessage.msgs = null;
			inst.h4 = "<i class='material-icons'>update</i> Modification - "+inst.type+" : ";
			this.installationtoOperate = inst
			$('#modal').modal('open');
			$('#ionom').focus();
		},

		//updateInstallation
		updateInstallation: function(event){
			event.preventDefault();

			var refs = this.$refs
			var inst = {
				nom: refs.instnom.value,
				arrondissement: refs.instarr.value
			}
			refs.instdata.forEach(function(elem){
				inst[elem.id] = elem.value
			});

			//---put--
			this.$http({
					method: 'put',
					url: this.updateInstallationApiUri+this.installationtoOperate._id,
					data: inst
				})
				.then(
					(response) => {
						this.formMessage.failed = false;
						this.formMessage.msgs = "Modification effectuée avec succès!"
						this.updateInstallationtoOperate()
						this.searchByArr() //assuming this.instasearch not change
					},
					(error) => {
						this.formMessage.failed = true;
						this.formMessage.msgs = '- '+error.response.data.message
						var shemaerrz = ''
						if(error.response.data.result.errors && error.response.data.result.errors.constructor === Array){
							error.response.data.result.errors.forEach(function(err){
								if(err.argument == "date-time"){
									shemaerrz += ' - Date format invalid! Utiliser le format: 2017-03-07T13:27:43.000Z'
								}else{ shemaerrz += ' - '+err.message }
							})
						}
						this.formMessage.msgs += shemaerrz
					}
				)
		},

		//-- updateInstallationtoOperate
		updateInstallationtoOperate: function(){
			var vm = this
			this.$http.get(this.installationByIdApiUri+this.installationtoOperate._id)
				.then(function (response) {
					response.data.h4 = "<i class='material-icons'>update</i> Modification - "+response.data.type+" : ";
					Vue.set(vm.$data, 'installationtoOperate', response.data)
				}).catch(error => { console.log(error) });
		},

		//-- deleteInstallationInit
		deleteInstallationInit: function(inst){
			this.updatefield = false
			this.showModalDelbtn = true
			this.formMessage.msgs = null;
			inst.h4 = "<i class='material-icons'>delete</i> Suppression - "+inst.type+" : ";
			this.installationtoOperate = inst
			$('#modal').modal('open');
		},

		//-- deleteInstallation
		deleteInstallation: function(){
			//---delete--
			this.$http({
					method: 'delete',
					url: this.deleteInstallationApiUri+this.installationtoOperate._id
				})
				.then(
					(response) => {
						this.formMessage.failed = false
						this.showModalDelbtn = false
						this.formMessage.msgs = "Suppression effectuée avec succès!"
						this.searchByArr() //assuming this.instasearch not change
					},
					(error) => {
						this.formMessage.failed = true
						this.showModalDelbtn = false
						this.formMessage.msgs = error.response.data.message
					}
				);
		}
	}
});

$(document).ready(function() {
	$('.modal').modal();
});