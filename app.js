var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var index = require('./routes/index');
var users = require('./routes/users');
var api = require('./routes/api');
var auth = require('./routes/auth');


// Sportia requiring--------
const passport = require('passport'),
  session = require('express-session'),
  sportiaBD = require('./inc/db'),
  cron = require('node-cron'),
  importationTask = require('./inc/importationProcess'),
  helper = require('./helper'),
  frequence = helper.configs().importationCronTaskFrequence;


var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


// Session middleware //Sportia
app.use(session({secret: 'Sportia app', resave: true, saveUninitialized: true}));

// Passport middleware //Sportia
app.use(passport.initialize());
app.use(passport.session());

// Routes middleware
app.use('/', index);
app.use('/users', users);
app.use('/api', api);
app.use('/auth', auth);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  data = {
    title: 'Sportia',
    isAuth: helper.configs().forceAccessToAdminOptions ? true : req.isAuthenticated(),
    isRealyAuth: req.isAuthenticated(),
    user:  req.user
  }
  next(err, data);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

// Sportia-------------------------
// Run one importation when app start
importationTask.processImportation();

// Start importation cron task
var task = cron.schedule(frequence, function() { //---- schedule the importation task
	importationTask.processImportation();
}, false);
task.start();

module.exports = app;