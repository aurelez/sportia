const express = require('express'),
	router = express.Router(),
	restapi = require('../inc/restapi');

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('Please check Sportia api documentation');
});

//-- liste des installations pour un arrondissement spécifié en paramètre
router.get('/installations', restapi.getInstallationByArrond);

//-- liste des installations en mauvaise condition JSON
router.get('/badconditioninstallations/json', restapi.getBadconditionInstallationJson);

//-- modifier l'état d'une glissade
router.put('/installation/update/:id', restapi.updateInstallation);

//-- suprimer une glissade
router.delete('/installation/delete/:id', restapi.deleteInstallation);

//-----------------------------------------------------
//-- liste des noms darrondissement disponibles
router.get('/distinctsarrond', restapi.getDistinctArrond);

//-- liste des noms des installations disponibles
router.get('/installationsnames', restapi.getInstallationsNames);

//-- trouver une installation par ID
router.get('/installationbyid/:id', restapi.installationById);

//-- trouver une installation par nom
router.get('/installationbyname/:nom', restapi.installationByName);

module.exports = router;
