/*	Projet: NF4375 - Aut 2017
 	Author: Aurele Zannou - ZAND17058708
*/
const express = require('express'),
	router = express.Router(),

	installationController = require('../controllers/installationController'),
	logController = require('../controllers/logController'),
	docController = require('../controllers/docController');

/* GET home page. */
router.get('/', installationController.homeStart);

//-- Doc page
router.get('/doc', docController.getdoc);

//-- Logs page
router.get('/logs', logController.renderLogs);

module.exports = router;
