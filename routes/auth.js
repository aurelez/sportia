/*	Projet: NF4375 - Aut 2017
 	Author: Aurele Zannou - ZAND17058708
*/
const express = require('express'),
	router = express.Router(),

	twitterAuth = require('../inc/auth/twitterAuth'),
	logController = require('../controllers/logController');


//-- twitter auth
router.get('/twitter', twitterAuth.passport.authenticate('twitter'));

//-- twitter auth callback
router.get('/twitter/cb', twitterAuth.passport.authenticate('twitter', { failureRedirect: '/' }),
	function(req, res) {
		// Successful auth
		res.redirect('/');
	}
);

//-- logout
router.get('/logout', 
	function(req, res){
		//-- weritelog
		logController.write('Logout', 'Twitter logout: '+req.user.username);

		req.logout();
		res.redirect('/');
	}
);

module.exports = router;