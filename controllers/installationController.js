/*	Projet: NF4375 - Aut 2017
 	Author: Aurele Zannou - ZAND17058708
*/
const Installation = require('../models/installationModel'),
	helper = require('../helper');

module.exports = {
	//--homeStart
	homeStart: function(req, res) {
		data = {
			title: 'Sportia',
			isAuth: helper.configs().forceAccessToAdminOptions ? true : req.isAuthenticated(),
			isRealyAuth: req.isAuthenticated(),
			user:  req.user
		}
		res.render('index', {data: data});
	}
};