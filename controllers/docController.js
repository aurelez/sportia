/*	Projet: NF4375 - Aut 2017
 	Author: Aurele Zannou - ZAND17058708
*/
const raml2html = require('raml2html');

module.exports = {
	//--get doc
	getdoc: function(req, res){
		const defaultTheme = raml2html.getConfigForTheme();

		raml2html.render('views/apiDoc.raml', defaultTheme).then(function(result){
			res.setHeader('Content-Type', 'text/html');
			res.charset = 'utf-8';
			return res.status(200).send(result);
		}, function(error) {
			return console.error('raml2html.render:', error);
		});
	}
}