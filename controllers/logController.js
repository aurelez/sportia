const Log = require('../models/logModel'),
	helper = require('../helper');

module.exports = {
	write: function(type, desc, date = new Date().toLocaleString()){
		var log = new Log({
			type: type,
			desc: desc,
			date: date
		});
		log.save(function(err){ if(err) console.error(err); } );
	},

	renderLogs: function(req, res){
		data = {
			title: 'Sportia logs',
			isAuth: helper.configs().forceAccessToAdminOptions ? true : req.isAuthenticated(),
			isRealyAuth: req.isAuthenticated(),
			user:  req.user
		}

		Log.find({}).sort({_id: -1}).exec(function(err, results) {
			if (err) return handleError(err);
			data.logs = results;
	    	res.render('logs', {data: data});
		});
	}
}