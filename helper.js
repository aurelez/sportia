/*	Projet: NF4375 - Aut 2017
 	Author: Aurele Zannou - ZAND17058708
*/
module.exports = {
	// get configs.yaml content
	configs: function(){
		const yaml = require('js-yaml');
		const fs = require('fs');

		try {
		    return yaml.safeLoad(fs.readFileSync('configs.yml', 'utf8'));
		} catch (e) {
		    console.error(e);
		    return false;
		}
	}
}