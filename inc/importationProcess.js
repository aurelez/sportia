/*	Projet: NF4375 - Aut 2017
 	Author: Aurele Zannou - ZAND17058708
*/
const request = require('request'),
	csvtojson = require('csvtojson'),
	async = require('async'),
	xml2json = require('xml2js').parseString,
	mongoose = require('mongoose'),

	helper = require('../helper'),
	mailling = require('./mailling'),
	twitterInterface = require('./twitter'),
	Installation = require('../models/installationModel'),
	logController = require('../controllers/logController'),
	createBadCondInstallationsFiles = require('./createBadCondInstallationsFiles'),
	configs = helper.configs();


module.exports = {
	//--processImportation--------------------------------------------------
	processImportation: function(){ //the main function
		//-- weritelog
		logController.write('Notification', 'Start importation...');

		//-- var
		var csvPiscineslist = [],
			xmlPatinoiresList = [],
			xmlGlissadesList = [],
			newsInstallation = [];

		async.parallel([
			//-- getPiscines: Get picines from csv link-------
		    function(callback){
				csvtojson()
				.fromStream(request.get(configs.linkPiscines))
				.on('error',(error)=>{
					logController.write('Error', 'csvtojson picines failed: '+error);
				    return console.error('csvtojson picines failed:', error);
				})
				.on('json', (piscineRow)=>{
					temp = {
						nom: piscineRow.NOM,
						type: 'piscine',
						arrondissement: piscineRow.ARRONDISSE,
						data: {
							lat: piscineRow.LAT,
							long: piscineRow.LONG,
							equipement: piscineRow.EQUIPEME,
							point_x: piscineRow.POINT_X,
							point_y: piscineRow.POINT_Y,
							gestion: piscineRow.GESTION,
							propriete: piscineRow.PROPRIETE,
							adresse: piscineRow.ADRESSE,
							type: piscineRow.TYPE,
							id_uev: piscineRow.ID_UEV
						}
					};
					csvPiscineslist.push(temp);
				})
				.on('done', (error)=>{
					callback(null, csvPiscineslist);
				})
			},

			//-- get patinoires from xml link in parallel-------
			function(callback){
				var options = {
					url: configs.linkPatinoires,
					method: 'GET',
					headers: {
						'Content-Type': 'application/xml; charset=utf-8'
					}
				};
				request(options, function(error, response, body){
					if(error){
						logController.write('Error', 'request patinoires failed: '+error);
						return console.error('request patinoires failed:', error);
					}
				    
			    	xml2json(body, function (err, result) {
						if(error) return console.error('xml2js patinoires failed:', error);

				    	result.patinoires.patinoire.forEach(function(patinoire){
							temp = {
								nom: patinoire.nom[0],
								type: 'patinoire',
								arrondissement: patinoire.arrondissement[0].nom_arr[0],
								data: {
									condition: patinoire.condition[0],
									resurface: patinoire.resurface[0],
									arrose: patinoire.arrose[0],
									deblaye: patinoire.deblaye[0],
									ouvert: patinoire.ouvert[0],
									cle_arr: patinoire.arrondissement[0].cle[0],
									date_maj: new Date(patinoire.arrondissement[0].date_maj[0])
								}
							};
							xmlPatinoiresList.push(temp);
				    	});
				    	callback(null, xmlPatinoiresList);
					});
				});
			},

			//-- get glissade from xml link in parallel too-------
			function(callback){
				var options = {
					url: configs.linkGlissades,
					method: 'GET',
					headers: {
						'Content-Type': 'application/xml; charset=utf-8'
					}
				};
				request(options, function(error, response, body){
					if(error){
						logController.write('request gissades failed: '+error);
						return console.error('request gissades failed:', error);
					}

			    	xml2json(body, function (err, result) {
			    		if(error) return console.error('xml2js gissades failed:', error);

				    	result.glissades.glissade.forEach(function(glissade){
							temp = {
								nom: glissade.nom[0],
								type: 'glissade',
								arrondissement: glissade.arrondissement[0].nom_arr[0],
								data: {
									condition: glissade.condition[0],
									deblaye: glissade.deblaye[0],
									ouvert: glissade.ouvert[0],
									cle_arr: glissade.arrondissement[0].cle[0],
									date_maj: new Date(glissade.arrondissement[0].date_maj[0])
								}
							};
							xmlGlissadesList.push(temp);
				    	});
				    	callback(null, xmlGlissadesList);
					});
				});
			}
		],

		//--callback
		function(error, results){
		    //-- we are here only if function 1,2,3 called callback.
		    if(error) return console.error('processImportation failed:', error);

		    const freshInstallations = results[0].concat(results[1], results[2]);

			//-- compare to what we got in BD (finding news installations)
			Installation.find({}, function(err, docs){
				if(err) return console.error('Installation.findOne failed:', err);

				if(docs.length == 0){
					newsInstallation = freshInstallations.slice(); // slice because arrayA = arrayB is by referencing
				}else{
					freshInstallations.forEach(function(freshInstallation){
						if( isNewInstallation(freshInstallation, docs) && isNotAlreadyFound(freshInstallation, newsInstallation) ){
							newsInstallation.push(freshInstallation);
						}
					});
				}

				// drop installations from BD and insert what we got from montreal site
				mongoose.connection.db.listCollections({name: 'installations'})
			    .next(function(err, c_info) {
			        if(c_info){ // if collection installations exist, drop before reload new one
						mongoose.connection.db.dropCollection('installations', function(error, result){
							if(error) return console.error('installations.drop failed:', error);

							importInstallation(freshInstallations);
							//-- Alert notification with news installation that we found
							newsInstallationsAlert(newsInstallation, null);
							//console.log('drop')
						});
			        }else{
			        	msg = "<p>Sportia a détecté qu'il s'agit de votre première importation, Sportia vous avisera lorsque de nouvelles installations différentes de celles-ci seront détectées lors de la prochaine importation. Elles seront aussi automatiquement publiées sur un compte twitter.</p>";
			        	importInstallation(freshInstallations);
						//-- Alert notification with news installation that we found
						newsInstallationsAlert(newsInstallation, msg);
			        }
			    });
			});
		});
	}
}

//-- importInstallation
function importInstallation(freshInstallations){
	//----insert (importation)
	async.each(freshInstallations, function (inst, callback){
		installation = new Installation(inst);
		installation.save(function(err){
			if(err) callback(err);
			callback();
		});

	}, function(err){
		if(err) return console.error('installation.save failed:', err);

		//-- weritelog
		logController.write('Notification', 'End importation!');
		console.log('Importation Done!');

		//-- ok, now create badcondition csv and xml file
		createBadCondInstallationsFiles.createFiles();
	});
}

//-- newsInstallationsAlert (notification par courriel)
function newsInstallationsAlert(newsInstallation, firstImportMsg){
	//console.log('news', newsInstallation.length);
	if(newsInstallation.length > 0){
		var message = "Bonjour, <br>";
		var newsFoundMsg = "<p>Sportia a détecté de nouvelles installations depuis la dernière importation: <br>";
		newsFoundMsg += "Et automatiquement publiées sur le compte twitter: <a href='"+ configs.twitter.account.link +"'>"+ configs.twitter.account.link + "</a></p>"; 

		message += "<div style='max-width:680px;'>";
		message += firstImportMsg ? firstImportMsg : newsFoundMsg;

		newsInstallation.forEach(function(newsInstall){
			message += "<div style='background:#eee; border:1px solid #ccc; padding:10px; margin-bottom:5px;'>";
			message += "<b>Nom: </b>"+newsInstall.nom+"<br><b>Catégorie: </b>"+newsInstall.type+"<br><b>Arrondissement: </b>"+newsInstall.arrondissement+"<br>";

			for (var i in newsInstall.data) {
				if( newsInstall.data.hasOwnProperty(i) ) {
					message += "<b>"+i+"</b> :"+newsInstall.data[i]+"<br>";
				}
			}
			
			message += "</div>";

			//--twitter notification
			if(!firstImportMsg){ // Si pas la premiere importation
				twitt = "INF4375 - Sportia, new installation detected: " + newsInstall.nom;
				twitt = twitt.substring(0,139);
				twitterInterface.postTweet(twitt);
			}
		});

		message += '</div>';
		//-- email notification
		mailling.sendmail(null, configs.adminMail, 'Sportia - Nouvelles installations détectées', message);
	}
}

//-- isNotAlreadyFound
function isNotAlreadyFound(freshInstallation, newsInstallation){
	var res = true;
	res = newsInstallation.every(function(item){
		if(freshInstallation.nom == item.nom && freshInstallation.type == item.type && freshInstallation.arrondissement == item.arrondissement){
			return false;
		}
		return true;
	});
	return res;
}

//-- isNewInstallation
function isNewInstallation(freshInstallation, docs){
	var res = docs.every(function(doc){
		if(freshInstallation.nom == doc.nom && freshInstallation.type == doc.type && freshInstallation.arrondissement == doc.arrondissement){
			return false;
		}
		return true;
	});
	return res;
}