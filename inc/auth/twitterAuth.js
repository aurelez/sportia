/*	Projet: NF4375 - Aut 2017
 	Author: Aurele Zannou - ZAND17058708
*/
const passport = require('passport'),
	TwitterStrategy = require('passport-twitter').Strategy,

	User = require('../../models/userModel'),
	configs = require('../../helper').configs(),
	logController = require('../../controllers/logController'),
	hostpath = configs.app.isonline ? configs.app.onlinehost : configs.app.localhost;


//-- npm passport: https://github.com/jaredhanson/passport#search-all-strategies
//-- seriderinit
function seriderinit(){
	passport.serializeUser(function(user, done) {
		done(null, user.id);
	});

	passport.deserializeUser(function(id, done) {
		User.findById(id, function (err, user) {
			done(err, user);
		});
	});
}

//-- passport
passport.use(new TwitterStrategy({
		consumerKey: configs.twitter.api.consumer_key,
		consumerSecret: configs.twitter.api.consumer_secret,
		callbackURL: hostpath+configs.twitter.api.callback_url
	},
	function(token, tokenSecret, profile, cb) {

		var u_updates = {
			twitterId: profile.id,
			username: profile.username,
			displayname: profile.displayName,
			photo: profile.photos[0].value
		};

		var opts = {
			new: true, // neccessaire pour que findOneAndUpdate retourne pas null lorsque le doc existait pas
			upsert: true
		};

		// update or create user
		User.findOneAndUpdate({twitterId: profile.id}, u_updates, opts, function(err, user) {
			if(err) {
				return cb(err);
			}else{
				//-- weritelog
				logController.write('Login', 'Login with Twitter: '+profile.username);
				return cb(null, user);
			}
		});
	}
));

//serialize/desirialize user in session
seriderinit();

module.exports = {
	passport: passport
}