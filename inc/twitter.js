/*	Projet: NF4375 - Aut 2017
 	Author: Aurele Zannou - ZAND17058708
*/
const logController = require('../controllers/logController');

module.exports = {

	postTweet: function(twitt){
		const Twitter = require('twitter'),
			helper = require('../helper');

		var client = new Twitter(helper.configs().twitter.api);

		client.post('statuses/update', {status: twitt},  function(error, tweet, response) {
			if(error){
				logController.write('Error', 'Twitter: '+error);
				return console.error('Twitter post failed:', error);
			}
			logController.write('Notification', 'Twitt posted: '+twitt);
		});
	}
}