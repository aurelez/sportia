/*	Projet: NF4375 - Aut 2017
 	Author: Aurele Zannou - ZAND17058708
*/
const jsonschema = require('jsonschema'),

	mongoose = require('mongoose'),
	schemasSportia = require('../inc/schemas'),
	Installation = require('../models/installationModel'),
	configs = require('../helper').configs();

module.exports = {
	//-- getInstallationByArrond
	getInstallationByArrond: function(req, res){
		if(!req.query.arrondissement) return res.status(400).json({message: '400! Bad Request! Recherche vide, Go to /doc for documentation.'});

		Installation.find({arrondissement: new RegExp('^'+req.query.arrondissement+'$', "i")}, {'__v': 0}, function(err, docs){
			if(err) return res.status(500).json({message: 'Error 500: Something broke!'});

			res.setHeader('Content-Type', 'application/json');
			return res.status(200).json(docs);
		});
	},

	//-- list installations en bad condition JSON
	getBadconditionInstallationJson: function(req, res){
		Installation.find({'data.condition': 'Mauvaise'}, {'__v': 0}).sort({nom: 1}).exec(function(err, docs){
			if(err) return res.status(500).json({message: 'Error 500: Something broke!'});

			res.setHeader('Content-Type', 'application/json');
			return res.status(200).json(docs);
		});
	},

	//-- modifier l'état d'une installation
	updateInstallation: function(req, res){
		//--if(!req.params.id) return res.status(400).send('Bad Request! Missing params.id (installation object ID).'); // catched by 404
		if(req.body == null) return res.status(400).json({message: '400! Bad Request! Missing installation object.'});

		//-- check valid object ID
		if(!mongoose.Types.ObjectId.isValid(req.params.id)) 
			return res.status(400).json({message: 'Bad Request! your objectId ('+req.params.id+') is not valid!'});
		
		//-- now work on it
		Installation.findById(req.params.id, function(err, doc){
			if(err) return res.status(500).json({message: 'Error 500: Something broke!'});
			if(doc == null) return res.status(404).json({message: "Sportia didn't find an installation with the ID: "+req.params.id});

			//check-empty fields
			if(req.body.nom.length < 1 || req.body.arrondissement < 1)
				return res.status(404).json({message: "Les champs (nom et arrondissement) ne peuvent être vide!"});

			//-- check if its glissade
			if(doc.type == 'glissade'){
				var result = jsonschema.validate(req.body, schemasSportia.updateGlissade);
				if(result.errors.length > 0) return res.status(400).json({'message': 'Error glissade Json Schema', 'result': result});

				doc.data = {
					cle_arr: req.body.cle_arr,
					condition: req.body.condition,
					date_maj: req.body.date_maj,
					deblaye: req.body.deblaye,
					ouvert: req.body.ouvert
				};
			}

			//-- check if its patinoire
			if(doc.type == 'patinoire'){
				var result = jsonschema.validate(req.body, schemasSportia.updatePatinoire);
				if(result.errors.length > 0) return res.status(400).json({'message': 'Error patinoire Json Schema', 'result': result});

				doc.data = {
					cle_arr: req.body.cle_arr,
					condition: req.body.condition,
					date_maj: req.body.date_maj,
					deblaye: req.body.deblaye,
					ouvert: req.body.ouvert,
					resurface: req.body.resurface,
					arrose: req.body.arrose
				};
			}

			//-- check if its piscine
			if(doc.type == 'piscine'){
				var result = jsonschema.validate(req.body, schemasSportia.updatePiscine);
				if(result.errors.length > 0) return res.status(400).json({'message': 'Error piscine Json Schema', 'result': result});

				doc.data = {
					lat: req.body.lat,
					long: req.body.long,
					equipement: req.body.equipement,
					point_x: req.body.point_x,
					point_y: req.body.point_y,
					gestion: req.body.gestion,
					propriete: req.body.propriete,
					adresse: req.body.adresse,
					type: req.body.type,
					id_uev: req.body.id_uev
				};
			}

			doc.nom = req.body.nom;
			doc.arrondissement = req.body.arrondissement;
			//-- now update
			doc.save(function(err){
				if(err) return res.status(500).json({message: 'Error 500: Something broke!'});
				//--  done
				return res.status(200).json({message: 'The installation ('+req.params.id+') is correctly updated.'});
			});
		});
	},

	//-- supprimer une installation
	deleteInstallation: function(req, res){
		//-- check valid object ID
		if(!mongoose.Types.ObjectId.isValid(req.params.id)) 
			return res.status(400).json({message: 'Bad Request! your objectId ('+req.params.id+') is not valid!'});

		Installation.findById(req.params.id, function(err, doc){
			if(err) return res.status(500).json({message: 'Error 500: Something broke!'});

			if(doc == null) return res.status(404).json({message: "Sportia didn't find an installation with the ID: "+req.params.id});

			//-- now delete
			doc.remove(function(err){
				if(err) return res.status(500).json({message: 'Error 500: Something broke!'});
				//--  done
				return res.status(200).json({message: 'The installation ('+req.params.id+') is correctly removed.'});
			});
		});
	},

	//-- getDistinctArrond-------------------------------------------------
	getDistinctArrond: function(req, res){
		Installation.find().distinct('arrondissement', function(err, docs){
			if(err){
				res.status(500).json({message: 'Error 500: Something broke!'});
			}else{return res.status(200).json(docs)}
		});
	},

	//-- getInstallationsNames
	getInstallationsNames: function(req, res){
		Installation.find().distinct('nom', function(err, docs){
			if(err){
				res.status(500).json({message: 'Error 500: Something broke!'});
			}else{return res.status(200).json(docs.sort())}
		});
	},

	//-- installationById
	installationById: function(req, res){
		if(!mongoose.Types.ObjectId.isValid(req.params.id)) 
			res.status(400).json({message: 'Bad Request! your objectId ('+req.params.id+') is not valid!'});

		Installation.findById(req.params.id, function(err, doc){
			if(err){
				res.status(500).json({message: 'Error 500: Something broke!'});
			}else{
				if(doc == null) 
					res.status(404).json({message: "Sportia didn't find an installation with the ID: "+req.params.id});
				return res.status(200).json(doc)
			}
		});
	},

	//-- installationByName
	installationByName: function(req, res){
		console.log(req.params.nom)
		Installation.find({'nom': req.params.nom}, function(err, docs){
			if(err){
				res.status(500).json({message: 'Error 500: Something broke!'});
			}else{
				if(docs == null) 
					res.status(404).json({message: "Sportia didn't find an installation with the name: "+req.params.nom});
				return res.status(200).json(docs)
			}
		});
	}
}