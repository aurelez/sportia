/*	Projet: NF4375 - Aut 2017
 	Author: Aurele Zannou - ZAND17058708
*/
const xmlify = require('xmlify'),
	csvify = require('json2csv'),
	fs = require('fs'),
	Installation = require('../models/installationModel'),
	logController = require('../controllers/logController');

module.exports = {
	createFiles: function(){
		// create dadcondinstallation csv and xml
		Installation.find({'data.condition': 'Mauvaise'}, {'__v': 0}).sort({nom: 1}).exec(function(err, docs){

			var rawdocs = [];
			var fields = [
				"_id",
				"mom",
				"type",
				"arrondissement",
				"cle_arr",
				"date_maj",
				"condition",
				"deblaye",
				"ouvert",
				"resurface",
				"arrose",
				"lat",
				"long",
				"equipement",
				"point_x",
				"point_y",
				"gestion",
				"propriete",
				"adresse",
				"piscine_type",
				"id_uev"
			];

			docs.forEach(function(doc){
				rawdocs.push({
					_id: doc._id,
					mom: doc.nom,
					type: doc.type,
					arrondissement: doc.arrondissement,
					cle_arr: doc.data.cle_arr,
					date_maj: doc.data.date_maj,
					condition: doc.data.condition,
					deblaye: doc.data.deblaye,
					ouvert: doc.data.ouvert,
					resurface: doc.data.resurface,
					arrose: doc.data.arrose,
					lat: doc.data.lat,
					long: doc.data.long,
					equipement: doc.data.equipement,
					point_x: doc.data.point_x,
					point_y: doc.data.point_y,
					gestion: doc.data.gestion,
					propriete: doc.data.propriete,
					adresse: doc.data.adresse,
					piscine_type: doc.data.piscine_type,
					id_uev: doc.data.id_uev
				})
			});

			//-- create installations en bad condition CSV
			var csvDocs = csvify({ data: rawdocs, fields: fields });
			fs.writeFile('public/localfiles/badconditionpiscines.csv', csvDocs, function(err) {
				logController.write('Notification', 'Create badconditionpiscines.csv Done!');
				if (err){
					console.error('Failed to create bad condition csv file:', err);
					logController.write('Error', 'Failed to create bad condition csv file '+err);
				}
			});

			//-- list installations en bad condition XML
			var xmlDocs = xmlify(docs, 'installations');
			fs.writeFile('public/localfiles/badconditionpiscines.xml', xmlDocs, function(err) {
				logController.write('Notification', 'Create badconditionpiscines.xml Done!');
				if (err){
					console.error('Failed to create bad condition xml file:', err);
					logController.write('Error', 'Failed to create bad condition xml file '+err);
				}
			});
		});
	}
}