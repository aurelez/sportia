/*	Projet: NF4375 - Aut 2017
 	Author: Aurele Zannou - ZAND17058708
*/
module.exports = {
	updateGlissade: {
		type: "object",
		required: true,
		additionalProperties: true,
	    properties: {
			nom: {
				type: "string",
				required: true
			},
			arrondissement: {
				type: "string",
				required: true
			},
			cle_arr: {
				type: "string",
				required: false
			},
			condition: {
				type: "string",
				required: false
			},
			date_maj: {
				type: "string",
				format: "date-time",
				required: false
			},
			deblaye: {
				type: "string",
				required: false
			},
			ouvert: {
				type: "string",
				required: false
			}
		}
	},

	updatePatinoire: {
		type: "object",
		required: true,
		additionalProperties: false,
	    properties: {
			nom: {
				type: "string",
				required: true
			},
			arrondissement: {
				type: "string",
				required: true
			},
			cle_arr: {
				type: "string",
				required: false
			},
			condition: {
				type: "string",
				required: false
			},
			date_maj: {
				type: "string",
				format: "date-time",
				required: false
			},
			deblaye: {
				type: "string",
				required: false
			},
			resurface: {
				type: "string",
				required: false
			},
			arrose: {
				type: "string",
				required: false
			},
			ouvert: {
				type: "string",
				required: false
			}
		}
	},

	updatePiscine: {
		type: "object",
		required: true,
		additionalProperties: true,
	    properties: {
			nom: {
				type: "string",
				required: true
			},
			arrondissement: {
				type: "string",
				required: true
			},
			lat: {
				type: "string",
				required: false
			},
			long: {
				type: "string",
				required: false
			},
			point_x: {
				type: "string",
				required: false
			},
			point_y: {
				type: "string",
				required: false
			},
			equipement: {
				type: "string",
				required: false
			},
			gestion: {
				type: "string",
				required: false
			},
			propriete: {
				type: "string",
				required: false
			},
			adresse: {
				type: "string",
				required: false
			},
			type: {
				type: "string",
				required: false
			},
			id_uev: {
				type: "string",
				required: false
			}
		}
	}
}

/*
{
  "nom": "A11",
  "arrondissement": "inkauzArrondissement",
  "cle_arr": "inkauzClearr",
  "condition": "Mauvaise",
  "date_maj": "2018-03-04T14:50:08.000Z",
  "deblaye": "InkauzDeblayeee",
  "ouvert": "InkauzOuvert"
}
*/