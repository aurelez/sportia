/*  Projet: NF4375 - Aut 2017
    Author: Aurele Zannou - ZAND17058708
*/
const nodemailer = require('nodemailer'),
    helper = require('../helper'),
    logController = require('../controllers/logController');

var transporter = nodemailer.createTransport(helper.configs().mailconfigs);

module.exports = {
    sendmail: function(from, to, subject, message){
        from = from ? from : '"Sportia Admin" <'+helper.configs().mailconfigs.auth.user+'>';
        var options = {
            from: from,
            to: to,
            subject: subject,
            html: message
        };

        transporter.sendMail(options, function(error, info){
            if (error) {
                logController.write('Error', 'Mail sending error: '+error);
                return console.error('mail sending error:', error);
            }
            logController.write('Notification', 'News installations notification sent to: '+to);
        });
    }
}