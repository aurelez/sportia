/*	Projet: NF4375 - Aut 2017
 	Author: Aurele Zannou - ZAND17058708
*/
const helper = require('../helper'),
	bdd = helper.configs().bdd;

const mongoose = require('mongoose');

const localBbURI = 'mongodb://'+bdd.local.host+':'+bdd.local.port+'/'+bdd.local.name;
const remoteBdURI = 'mongodb://'+bdd.remote.username+':'+bdd.remote.password+'@'+bdd.remote.host+':'+bdd.remote.port+'/'+bdd.remote.name;
const dbURI = bdd.useremote ? remoteBdURI : localBbURI;

mongoose.connect(dbURI, {useMongoClient: true});
mongoose.Promise = global.Promise; // stop Mongoose: mpromise DeprecationWarning

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

//-- we need to close
var closeConnection = function() { 
	mongoose.connection.close(function(){
		console.log('Mongoose: Bye!');
		process.exit(0);
	});
}
// Node process ends ? close mongoose connect
process.on('SIGINT', closeConnection).on('SIGTERM', closeConnection);

module.exports = db;